import { Body, Controller, Get, NotFoundException, Post } from '@nestjs/common';
import { OauthDTO } from 'src/domain/dto/oauth.dto';
import { UserDTO } from 'src/domain/dto/user.dto';
import { AuthenticationService } from '../services/authentication.service';

@Controller('authentication')
export class AuthenticationController {
  constructor(private authenticationService: AuthenticationService) {}

  @Post('/')
  async login(@Body() oauthDTO: OauthDTO): Promise<any> {
    console.log('desde controller', oauthDTO);
    const service = await this.authenticationService.authentication(oauthDTO);

    if (!service) throw new NotFoundException('data null');

    return service;
  }
}
