import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../domain/entities/user.entity';
import { OauthDTO } from '../../domain/dto/oauth.dto';
import { UserDTO } from '../../domain/dto/user.dto';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthenticationService {
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

  async authentication(oauthDTO: OauthDTO): Promise<any> {
    // async authentication(userDTO: UserDTO, oauthDTO: OauthDTO): Promise<User[]> {
    // const user = await this.userRepo.find({ nombres: createVoluntarioDTO.nombres,
    //   apellido_paterno : createVoluntarioDTO.apellido_paterno, apellido_materno: createVoluntarioDTO.rut_dns,
    //   correo: createVoluntarioDTO.correo, password: createVoluntarioDTO.password, campus: createVoluntarioDTO.campus,
    //   });

    const user = await this.userRepo.find({ correo: oauthDTO.email });
    const auth = [];
    // const user = await this.userRepo.find({
    //   where: oauthDTO,
    // });
    // console.log('eeees', user.length[0]);
    // if (user.length[0] == 'undefined') {
    //   console.log('sfsdfsd', user);
    // }

    const info = await user.map(function (object) {
      // user.map(async function (object) {
      const campare = bcrypt.compare(oauthDTO.password, object.password);

      const token = jwt.sign(
        {
          object,
        },
        'este-es-el-seed',
        { expiresIn: '48h' },
      );

      const verify = campare ? token : 'Password invalid';

      return verify;

      // return verify;
    });

    // const data = user.length[0] == '' ? 'not user' : info;
    // if (!user.correo) throw new NotFoundException('user not exits');
    // user.map(function (object) {
    //   object.correo
    // });
    // console.log('dfsdfsd', info);

    return info;

    // const password = userDTO.password;

    // const round = 10;
    // const passEncrip = await bcrypt.hash(password, round);

    // console.log('passenc', passEncrip);
    // const newUser = await this.userRepo.create({
    //   nombres: userDTO.nombres,
    //   apellido_paterno: userDTO.apellido_paterno,
    //   apellido_materno: userDTO.rut_dns,
    //   correo: userDTO.correo,
    //   password: userDTO.password,
    //   campus: userDTO.campus,
    // });

    // const register = await this.userRepo.save(newUser);

    // return register;
  }
}
