import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/domain/entities/user.entity';
import { AuthenticationController } from './controllers/authentication.controller';
import { AuthenticationService } from './services/authentication.service';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [AuthenticationService],
  controllers: [AuthenticationController],
})
export class FollowerModule {}
