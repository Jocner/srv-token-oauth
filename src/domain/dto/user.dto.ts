export class UserDTO {
  readonly nombres: string;
  readonly apellido_paterno: string;
  readonly apellido_materno: string;
  readonly rut_dns: string;
  readonly correo: string;
  readonly password: string;
  readonly campus: string;
  readonly privilegio: string;
}
