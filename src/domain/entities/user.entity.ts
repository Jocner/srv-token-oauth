import { Entity, ObjectID, ObjectIdColumn, Column } from 'typeorm';

@Entity()
export class User {

  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  nombres: string;

  @Column()
  apellido_paterno: string;

  @Column()
  apellido_materno: string;

  @Column()
  rut_dns: string;

  @Column()
  correo: string;

  @Column()
  password: string;

  @Column()
  campus: string;

  @Column()
  privilegio: string;

}
