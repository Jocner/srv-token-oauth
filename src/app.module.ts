import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FollowerModule } from './infrastructure/follower.module';
// import { env_vars } from './utils/enviroment';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: 'mongodb+srv://jocner:jocner@cluster0.axw6l.mongodb.net/swagger-typescript?retryWrites=true&w=majority',
      useNewUrlParser: true,
      logging: true,
      synchronize: false,
      entities: ['dist/**/**/*.entity.js'],
    }),
    FollowerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
