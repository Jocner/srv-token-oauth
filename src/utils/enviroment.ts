import * as dotenv from 'dotenv';

dotenv.config();

export const env_vars = {
  PORT: process.env.PORT,
  USER_DB: process.env.USER_DB,
  PASSWORD_DB: process.env.PASSWORD_DB,
  CONNECTSTRING: process.env.CONNECTSTRING,
};
